local conf = {
    black_list_rule = {
        limiter = {}
    }
}

-- 模拟黑名单，这个数据可以在数据库或者redis
local black_list = {
    "1", "2", "3", "4", "5"
}

-- who 方法
function black_list_who(limit) 

    local args = limit.args
    local user = args["user"]
    local reslut = false
    
    for _, item in pairs(black_list) do
        if item == user then
            result = true
            break
        end
    end

    return result, {rule = "black_list_rule", message = message, data = {user=user}}
end

-- 输出方法
function message(limit, data)
    -- 这里也可以出公告
    ngx.say("reject user: " .. data.user)
    ngx.exit(ngx.HTTP_OK)
end

local limit = require "limit":new(conf)

limit:who(black_list_who):execute()
