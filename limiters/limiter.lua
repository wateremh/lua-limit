--[[
    基础限流类，所有其他类均需要继承这个类，参考其他类文件开头写法

    这个类也可以放在限流链上使用，使用这个代表无条件拒绝流量。

    Author: mahaixing@gmail.com
    License: MIT
]]

local setmetatable = setmetatable
local assert = assert
local utils = require "utils"

local _M = {
    _VERSION = '0.0.1'
}

function _M:new(config) 
    local object = {
        conf = config
    }

    self.__index = self

    return setmetatable(object, self)
end

function _M:process_config()
    utils.log("processing config in limiter", ngx.INFO)
    return
end

function _M:execute() 
    utils.log("rejecting request in limiter", ngx.INFO)
    return true
end

return _M