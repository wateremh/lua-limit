--[[
    使用限流的时间范围，需要结合其他限流手段使用，用于在指定的时间范围内
    使用配置的限流手段对请求进行限流。
    
    Author: mahaixing@gmail.com
    License: MIT
]]

local assert = assert
local pairs = pairs
local utils = require("utils")

local _M = require("limiters.limiter"):new()

--[[
    处理配置文件
]]
function _M:process_config()
    -- 起始时间
    self.start_datetime = assert(self.conf["start_datetime"], "Please set start datetime!")
    -- 终止时间
    self.end_datetime = assert(self.conf["end_datetime"], "Please set end datetime!")
    -- 时间范围内的星期数，范围在1-7，1代表周一，以此类推
    -- 可以是数字，如 1；可以是 table，如：{1,2,3,7}
    if self.conf["week_day"] == nil then 
        self.week_day = nil 
    else 
        self.week_day = self.conf["week_day"] 
    
        if type(self.week_day) == "table" then 
            for _, item in pairs(self.week_day) do 
                if (item < 1 or item > 7) then
                    utils.log("week_day must between 1-7!", ngx.ERR)
                    error("week_day must between 1-7!")
                end
            end
        else
            if (self.week_day < 1 or self.week_day > 7) then
                utils.log("week_day must between 1-7!", ngx.ERR)
                error("week_day must between 1-7!")
            end
        end
    end
end

function _M:execute()
    local current_datetime = os.time()
    local current_week_day = os.date("%w")

    if (self.week_day ~= nil) then
        -- 当天是否是指定的星期几
        local in_week_day = false
        -- 如果定义的星期范围
        if (type(self.week_day) == "table") then 
            for _, item in pairs(self.week_day) do
                if (item == current_week_day) then in_week_day = true end
            end
        else
            -- 如果仅仅指定了具体星期几
            if self.week_day == current_week_day then in_week_day = true end
        end

        -- 如果不属于指定的星期几，流量直接放行
        if (in_week_day == false) then return false end
    end
    
    -- 解析开始日期
    local ok_start, start_datetime = pcall(os.time, self.start_datetime)
    
    if not ok_start then 
        utils.log("start_datetime config error: " .. start_datetime)
        error("start_datetime config error: " .. start_datetime)
        -- 出错就认为不在这个时间范围内
        return false
    end
    
    -- 解析截止时间
    local ok_end, end_datetime = pcall(os.time, self.end_datetime)
    if not ok_end then 
        utils.log("end_datetime config error: " .. end_datetime)
        error("end_date config error: " .. end_datetime)
        -- 出错就认为不在这个时间范围内
        return false
    end

    if (end_datetime <= start_datetime) then
        utils.log("end_datetime must bigger than start_datetime")
        error("end_datetime must bigger than start_datetime")
        return false
    end

    local result =  (os.difftime(current_datetime, start_datetime) >= 0 
                and os.difftime(current_datetime, end_datetime) <= 0  and true or false)
                
    -- 通常需要限流链中下一个节点进行具体的限流算法处理
    if result == true then
        return true, true
    else
        return false
    end
end

return _M