local conf = {
    default_rule = {
        bucket = {
            bucket_dict_name = "limit_srvs",
            bucket_rate = 2,
            bucket_burst = 1,
            --bucket_target = ”ip“
            bucket_target = "uri"
        }
    }
}

function message(limit, data)
    ngx.say("reject by bucket")
    ngx.exit(ngx.HTTP_OK)
end

-- 没有指定 who 方法，所以需要使用默认的 default_rule 规则链
local limit = require "limit":new(conf)
limit:execute()